# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class stock_picking(models.Model):

    _inherit = 'stock.picking'

    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    project_id = fields.Many2one(
        string='Project',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=_('Project id'),
        comodel_name='account.analytic.account',
        domain=lambda self: [('type', 'in', ['normal', 'contract'])],
        context={},
        ondelete='cascade',
        auto_join=False
    )
